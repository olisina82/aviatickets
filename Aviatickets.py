def main_menu():
    print('Главное меню:\n')
    print('1 - ввод рейса')
    print('2 - вывод всех рейсов')
    print('3 - поиск рейса по номеру')
    print('0 - завершение работы')


def flight_input():

    flight_number_invite = 'ХХХХ - номер рейса: '
    flight_number = check_count(4, flight_number_invite)

    flight_date_invite = 'ДД/ММ/ГГГГ - дата рейса: '
    flight_date = check_count(10, flight_date_invite)

    departure_time_invite = 'ЧЧ:ММ - время вылета: '
    departure_time = check_count(5, departure_time_invite)

    while True:
        flight_duration = input('ХХ.ХХ - длительность перелета: ')
        count = 0
        for index, i in enumerate(flight_duration):
            if 0 <= index < 2 and '0' <= i <= '9':
                count += 1
            elif 2 < index < 5 and '0' <= i <= '9':
                count += 1
            elif index >= 5:
                count += 1
            elif index == 2 and i == ':' or index == 2 and i == '.' or index == 2 and i == ',':
                count += 1
        if count != 5:
            print('Ошибка ввода. Поле должно содержать не отрицательное двузначное число')
        else:
            break

    departure_airport_invite = 'ХХХ - аэропорт вылета: '
    departure_airport = check_count(3, departure_airport_invite)

    arrival_airport_invite = 'ХХХ - аэропорт назначения: '
    arrival_airport = check_count(3, arrival_airport_invite)

    while True:
        ticket_cost = input('.Х - стоимость билета (> 0): ')
        count = -1
        index = 0
        for i_index, i in enumerate(ticket_cost):
            index = i_index
            if '0' <= i <= '9' or i == '.' or i == ',':
                count += 1
            else:
                count -= 1
        if count != index:
            print('Ошибка ввода. Поле должно содержать не отрицательное двузначное число')
        else:
            break

    flight_information = flight_number.upper() + ' ' + flight_date + ' ' + departure_time + ' ' \
                         + flight_duration + ' ' + departure_airport.upper() + ' ' + arrival_airport.upper() \
                         + ' ' + ticket_cost

    print(flight_information, 'добавлена')
    return flight_information


def check_count(value, invite):
    while True:
        subject = input(invite)
        count = 0
        for _ in subject:
            count += 1
        if count != value:
            print('Ошибка ввода. Поле должно содержать символов:', value)
        else:
            return subject
            break


print('Сервис поиска авиабилетов')
all_flight_information = ''
main_menu()
user_option = int(input('\nВведите номер пункта: '))

while True:
    while user_option == 1:
        all_flight_information += ' ' + flight_input() + '*'
        print()
        main_menu()
        user_option = int(input('\nВведите номер пункта: '))

    if user_option == 2 and all_flight_information == '':
        print('Информация о рейсах отсутствует')
        print()
        main_menu()
        user_option = int(input('\nВведите номер пункта: '))

    elif user_option == 2 and all_flight_information != '':
        print(all_flight_information)
        print()
        main_menu()
        user_option = int(input('\nВведите номер пункта: '))

    elif user_option == 3:
        answer = ''
        start_index = 0
        end_index = 0
        flight_user = input('Введите номер рейса в формате ХХХХ: ')

        for i_index, i in enumerate(all_flight_information):
            if i != ' ':
                answer += i
            elif answer == flight_user:
                answer = ''
                start_index = i_index
            else:
                answer = ''

        for i_index, i in enumerate(all_flight_information):
            if i_index >= start_index and i == '*':
                end_index = i_index
                break

        if start_index == 0:
            print(f'Рейс {flight_user} не найден')

        else:
            answer = ''
            for i_index, i in enumerate(all_flight_information):
                if end_index >= i_index >= start_index - 4:
                    answer += i

            print('Информация о рейсе:', answer)
        print()
        main_menu()
        user_option = int(input('\nВведите номер пункта: '))

    elif user_option == 0:
        break
    else:
        print('Ошибка ввода. Введите корректный пункт меню')

